<?php

  $host = '127.0.0.1';
  $db   = 'coursedb';
  $user = 'root';
  $pass = '';
  $charset = 'utf8';

	try {    

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $opt = array(
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, // выводить ошибки и исключения
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, // по умолчанию вытаскивать данные как ассоциир. массив
        //PDO::ATTR_EMULATE_PREPARES   => false, //объект данных php класс соедин php и бд
    );
    $pdo = new PDO($dsn, $user, $pass, $opt); //класс, который подключает к бд
	} catch (PDOException $e) {               //обработка ошибки класса PDOExce. 
		echo "ERROR CONNECT".$e->getMessage(); 
		exit();
	}

?>