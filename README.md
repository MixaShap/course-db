Курсовой проект базой данных на основе MySQL <br />
Основные запросы к базе данных, представленные в проекте:
* 	Создать отчет о работе водителей в марте 2017 года по форме: Уникальный номер водителя, фамилии водителя, общее количество отработанных часов.
```
SELECT `surname` AS 'Фамилия', SUM(HOUR(TIMEDIFF(`end_time`,`start_time`))) AS 'Количество часов' 
FROM `drivers` JOIN `departure` USING(`ID_driver`) WHERE MONTH(`start_time`)=3AND YEAR(`start_time`)=2017 GROUP BY `surname`;
```

* 	Покажите фамилии всех водителей, которые работали на маршруте с названием XXX в марте 2017 
```
SELECT `surname` AS 'Фамилия' FROM `drivers` JOIN `schedule` USING(`ID_driver`) WHERE MONTH(`month`)=3 AND YEAR(`month`)=2017 AND `ID_route`=XXX;
```
* 	Покажите фамилии водителей, которые пока не совершили ни одного выхода на работу.
```
SELECT `surname` AS 'Фамилия' FROM `drivers` LEFT JOIN `schedule` USING(`ID_driver`) WHERE `ID_schedule` IS NULL;
```
* 	Покажите фамилии водителей, которые ни разу не выходили на работу в марте 2013 года.
```
SELECT surname AS 'Фамилия' FROM drivers LEFT JOIN
(SELECT * FROM departure WHERE MONTH(start_time)=$date[1] AND YEAR(start_time)=$date[0])x
USING(ID_driver) WHERE start_time IS NULL;
```
* 	Покажите все сведения о водителе с самым большим стажем работы.
```
SELECT ID_driver AS 'ID Водителя', surname AS 'Фамилия', name AS 'Имя', dadName AS 'Отчество', address AS 'Адрес', recruit_date AS 'Дата принятия на работу', fired_date AS 'Дата увольнения' FROM `drivers` WHERE recruit_date IN (SELECT MIN(recruit_date) FROM drivers);
```
* 	Найти все сведения о водителе, который чаще всех работал на маршруте XXX в марте 2013 года (с использованием view). 
```
SELECT ID_driver AS 'ID Водителя', surname AS 'Фамилия', name AS 'Имя', dadName AS 'Отчество', address AS 'Адрес', recruit_date AS 'Дата принятия на работу', fired_date AS 'Дата увольнения' FROM drivers WHERE ID_driver IN ( SELECT ID_driver FROM courseView WHERE starts=( SELECT MAX(starts) FROM courseView));
```
VIEW:
```
CREATE VIEW courseview 
AS SELECT trip.id_Dr AS id_Dr, COUNT(0) AS starts 
FROM trip
WHERE ((trip.id_R = 2) AND (month(trip.Date_T) = 3) AND (year(trip.Date_T) = 2019))
GROUP BY trip.id_Dr;
```
* 	Хранимую процедуру, которая будет получать на вход месяц и год, и составлять статистический отчет “Сколько билетов продано по определенному маршруту за каждый месяц” на этот период.
```
    CREATE PROCEDURE tickets (in_month integer, in_year integer)
    BEGIN
      DECLARE flag, isReports, route, sum_ticket integer default 0;
      DECLARE C1 CURSOR FOR
        SELECT id_R, SUM(Last_ticket-First_ticket) FROM trip
          WHERE MONTH(Date_T)=in_month AND YEAR(Date_T)=in_year
          GROUP BY id_R;
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET flag=1;
      SELECT COUNT(*) FROM reports WHERE r_month=in_month AND r_year=in_year INTO isReports;
      IF isReports=0 THEN
      BEGIN
        OPEN C1;
        FETCH C1 INTO route, sum_ticket;
        WHILE flag = 0 DO
          INSERT INTO reports VALUES (null, route, sum_ticket, in_month, in_year);
          FETCH C1 INTO route, sum_ticket;
        END WHILE;
      SELECT "DONE!";
      CLOSE C1;
      END;
      ELSE
        SELECT "REPORT WAS CREATED ALREADY!";
      END IF;
    END
```


