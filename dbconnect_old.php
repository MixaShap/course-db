<?php
/*
Created by MixaShap 21.06.2017

Используется для подключения 
к базе данных на сервере
*/
	//header('Content-Type: text/html; charset=utf-8');

	ini_set("display_errors",1);
	error_reporting(E_ALL);

	$address_site = "";

	$mysqli = new mysqli("localhost","root","","coursedb");

    if (mysqli_connect_errno()) { 
        echo "<p><strong>Ошибка подключения к БД</strong>. Описание ошибки: ".mysqli_connect_error()."</p>";
        exit(); 
    } else {
 
    	// Устанавливаем кодировку подключения
    	$mysqli->set_charset('utf8');

		//echo "succesfully connected to DB";
	}
?>